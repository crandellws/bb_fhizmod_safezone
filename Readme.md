FhizMod - Fhizban's uMMORPG Modifications & Add-Ons
------------------------------------------------------------------------------------------

All modifications & add-ons require the uMMORPG Asset and assume that you use either a fresh
project or a correctly updated project. Modifications & Add-ons relate to the stated version
of uMMORPG. If you have any further questions, contact me on the unity forums.

Modifications require you to change the core script codes. Add-Ons on the other hand will
make use of the upcoming Add-On system.

* uMMORPG on the asset store: 	https://www.assetstore.unity3d.com/en/#!/content/51212
* uMMORPG official discussion: 	https://forum.unity3d.com/threads/ummorpg-official-thread.376636/
* uMMORPG official documentation:	https://noobtuts.com/unity/MMORPG

Check out all my uMMORPG Modfications & Add-Ons:	http://ummorpg.critical-hit.biz

It is recommended to backup your projects before applying any modifications to it!

SafeZone for uMMORPG 1.68
==========================================================================================

This simple script allows you to create a PvP (Player-vs-Player) SafeZone. While inside
a SafeZone, you are not able to use "Attack" type skills on other players. Note that players
outside the SafeZone can attack you - therefore the SafeZone should be rather large and
not directly adjacent to a normal zone (use a portal or teleporter).

1. Add a new 3d object to your project's object hierarchy.
1.a Check the "is Trigger" box in the objects Collider section in the Inspector.

2. Copy the file SafeZone.cs into your project's script folder.

3. Locate in your project's script files Entity.cs
3.1 Find this line:

	public abstract int manaMax{ get; }
	
	add this new line after it:
	
	[HideInInspector] public int safezone = 0;

3.2 Now find this line (in Entity.cs):

	// aggro ///////////////////////////////////////////////////////////////////
	
	add these new lines before it:
	
	// safezone ///////////////////////////////////////////////////////////////////
    // this function is called by the SafeZone (if any) on clients and server
    public virtual void OnSafeZoneEnter() {}
	public virtual void OnSafeZoneLeave() {}

3.3 Next, find this line (in Entity.cs):

	public abstract bool CanAttackType(System.Type type);
	
	add these new lines after it:
	
	// a new check to check for safezone
	public bool CastCheckSafeZone(Skill skill) {
		if ((target is Player) && skill.category == "Attack" && safezone != 0) {
			return false;
		} else {
			return true;
		}
	}

4. Locate in your project's script files Player.cs
4.1 Find this line:

	if (CastCheckSelf(skill) && CastCheckTarget(skill)) {
	
	replace it with this line:
	
	if (CastCheckSafeZone(skill) && CastCheckSelf(skill) && CastCheckTarget(skill)) {
	
4.2 Next, find this line (in Player.cs)

	agent.Warp(
	
	add this new line before it:
	
	safezone = 0;
	
4.3 Next, find this line (in Player.cs)

	// combat //////////////////////////////////////////////////////////////////
	
	add these new lines before it:
	
	// SafeZone ///////////////////////////////////////////////////////////////////
    // this function is called by the SafeZone (if any) on clients and server
    [ServerCallback]
    public override void OnSafeZoneEnter() {
        safezone = 1;
    }
    
    [ServerCallback]
    public override void OnSafeZoneLeave() {
        safezone = 0;
    }
    
    


That's it!

------------------------------------------------------------------------------------------