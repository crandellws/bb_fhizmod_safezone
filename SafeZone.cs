
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SphereCollider))] // SafeZone area trigger
public class SafeZone : MonoBehaviour {
    [SerializeField] Entity owner; // set in the inspector

    // same as OnTriggerStay
    void OnTriggerEnter(Collider co) {
        var entity = co.GetComponentInParent<Entity>();
        if (entity) owner.OnAggro(entity);
    }

    void OnTriggerStay(Collider co) {
        var entity = co.GetComponentInParent<Entity>();
        if (entity) owner.OnAggro(entity);
    }
}
